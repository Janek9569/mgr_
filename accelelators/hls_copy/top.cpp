#include <ap_fixed.h>
#include <ap_int.h>
#include <hls_fft.h>

struct wide_stream
{
	ap_uint<32> data;
	ap_uint<1> user;
	ap_uint<1> last;
};

void hls_copy(wide_stream* img, wide_stream* o1, wide_stream* o2)
{
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=img
#pragma HLS INTERFACE axis port=o1
#pragma HLS INTERFACE axis port=o2

#pragma HLS PIPELINE

   o1->data = img->data;
   o1->last = img->last;
   o1->user = img->user;

   o2->data = img->data;
   o2->last = img->last;
   o2->user = img->user;
}
