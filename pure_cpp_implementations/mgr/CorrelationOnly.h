#pragma once
#include "MovDetAlg.h"
#include <cstdint>

class CorrelationOnly :
	public MovDetAlg
{
public:
	CorrelationOnly(Mat& frame);
	~CorrelationOnly();
	virtual Mat calculate(Mat& newFrame);
private:
	void magSpectrums(InputArray _src, OutputArray _dst);
	void divSpectrums(InputArray _srcA, InputArray _srcB, OutputArray _dst, int flags, bool conjB);
	void fftShift(InputOutputArray _out);
	Point2d weightedCentroid(InputArray _src, cv::Point peakLocation, cv::Size weightBoxSize, double* response);

	static constexpr uint32_t L_X_SIZE = 128;
	static constexpr uint32_t L_Y_SIZE = 128;

	std::vector<Mat> smallImagesPrev;
	int n, m;
};

