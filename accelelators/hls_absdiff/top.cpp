#include <ap_fixed.h>
#include <ap_int.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#define ROWS 1080UL
#define COLS 1920UL
#define SIZE (ROWS*COLS)
#define BUFFER_SIZE (25600UL)
/*
void hls_absdiff(uint8_t *img1, uint8_t *img2, uint8_t *result)
{
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE m_axi port=img1 offset=slave
#pragma HLS INTERFACE m_axi port=img2 offset=slave
#pragma HLS INTERFACE m_axi port=result offset=slave

	uint8_t buf1[COLS] = { 0 };
	uint8_t buf2[COLS] = { 0 };
	uint8_t buf_res[COLS] = { 0 };

	for(int i = 0; i < ROWS; ++i)
	{
		memcpy(buf1, img1 + i*COLS, COLS);
		memcpy(buf2, img2 + i*COLS, COLS);

		{
			for(int j = 0; j < COLS; ++j)
			{
				#pragma HLS PIPELINE
				buf_res[j] = abs((int16_t)buf1[j] - (int16_t)buf2[j]);
			}
		}

		memcpy(result + i*COLS, buf_res, COLS);
	}
}*/

#define FROM_PTR_LAST_DATA ((SIZE / BUFFER_SIZE)*BUFFER_SIZE)
#define LAST_DATA_SIZE (SIZE - ((SIZE / BUFFER_SIZE)*BUFFER_SIZE))
void hls_absdiff(uint8_t *img1, uint8_t *img2, uint8_t *result)
{
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE m_axi port=img1 offset=slave
#pragma HLS INTERFACE m_axi port=img2 offset=slave
#pragma HLS INTERFACE m_axi port=result offset=slave

	uint8_t buf1[BUFFER_SIZE] = { 0 };
	uint8_t buf2[BUFFER_SIZE] = { 0 };
	uint8_t buf_res[BUFFER_SIZE] = { 0 };

	for(int i = 0; i < SIZE / BUFFER_SIZE; ++i)
	{
		memcpy(buf1, img1 + i*BUFFER_SIZE, BUFFER_SIZE);
		memcpy(buf2, img2 + i*BUFFER_SIZE, BUFFER_SIZE);

		for(int j = 0; j < BUFFER_SIZE; ++j)
		{
			#pragma HLS PIPELINE
			buf_res[j] = abs((int16_t)buf1[j] - (int16_t)buf2[j]);
		}

		memcpy(result + i*BUFFER_SIZE, buf_res, BUFFER_SIZE);
	}

	memcpy(buf1, img1 + FROM_PTR_LAST_DATA, LAST_DATA_SIZE);
	memcpy(buf2, img2 + FROM_PTR_LAST_DATA, LAST_DATA_SIZE);

	for(int j = 0; j < LAST_DATA_SIZE; ++j)
	{
		#pragma HLS PIPELINE
		buf_res[j] = abs((int16_t)buf1[j] - (int16_t)buf2[j]);
	}

	memcpy(result + FROM_PTR_LAST_DATA, buf_res, LAST_DATA_SIZE);
}
