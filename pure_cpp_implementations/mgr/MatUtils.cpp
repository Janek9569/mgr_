#include "MatUtils.h"
#include <fstream>
#include <iomanip>
#include <set>
MatUtils::MatUtils()
{
}


MatUtils::~MatUtils()
{
}

void MatUtils::printMat(Mat& mat, std::string fileName, std::string matName)
{
	std::fstream f(fileName, std::ios::out);
	if (!f.is_open())
		return;

	f << matName << ":=\n";
	for (int i = 0; i < mat.rows; ++i)
	{
		f << "[";
		for (int j = 0; j < mat.cols; ++j)
		{
			int value = (int)mat.at<float>(i, j);
			f << " " << std::setw(5) << value;
		}
		f << "]\n";
	}
}

std::vector<ValueWithPos> MatUtils::findBiggestValues(Mat & mat, int num)
{
	std::set<ValueWithPos, ValueWithPosCmp> set;
	std::vector<ValueWithPos> result;
	for (int i = 0; i < mat.rows; ++i)
	{
		for (int j = 0; j < mat.cols; ++j)
		{
			int value = (int)mat.at<float>(i, j);

			ValueWithPos actValue;
			actValue.point = Point2d(i, j);
			actValue.value = value;

			set.insert(actValue);
		}
	}

	int guard = 0;
	for (auto it = set.rbegin(); it != set.rend(); ++it)
	{
		if (guard >= num)
			break;

		result.push_back(*it);
		++guard;
	}

	return result;
}

std::vector<Mat> MatUtils::divideImage(Mat & mat, int xSize, int ySize)
{
	std::vector<Mat> smallImages;
	int rows = mat.rows;
	int columns = mat.cols;

	for (int y = 0; y < rows; y += ySize)
	{
		for (int x = 0; x < columns; x += xSize)
		{
			//TO DO: limit last frame
			cv::Rect rect = cv::Rect(x, y, xSize, ySize);
			smallImages.push_back(cv::Mat(mat, rect));
		}
	}
}
