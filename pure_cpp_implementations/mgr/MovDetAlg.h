#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;
class MovDetAlg
{
public:
	MovDetAlg(Mat& frame);
	~MovDetAlg();
	virtual Mat calculate(Mat& newFrame) = 0;
protected:
	Mat framePrev;
	MovDetAlg();
};

