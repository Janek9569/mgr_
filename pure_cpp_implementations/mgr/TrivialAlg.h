#pragma once
#include "MovDetAlg.h"
#include <cstdint>

class TrivialAlg :
	public MovDetAlg
{
public:
	TrivialAlg(Mat& frame);
	~TrivialAlg();
	virtual Mat calculate(Mat& newFrame);
private:
	static constexpr uint32_t GAUSSIAN_SIGMA_X = 25;
	static constexpr uint32_t GAUSSIAN_SIGMA_Y = 25;
};

