#include "top.h"

void hls_dilate_threshold(AXI_STREAM& img, AXI_STREAM& result)
{
    //Create AXI streaming interfaces for the core
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=img
#pragma HLS INTERFACE axis port=result

	BGR_IMAGE img_0(1080, 1920);
	BGR_IMAGE img_threshold(1080, 1920);
    BGR_IMAGE img_dilated1(1080, 1920);
    BGR_IMAGE img_dilated2(1080, 1920);


#pragma HLS dataflow

    hls::AXIvideo2Mat(img, img_0);
	// Take difference between frames
    hls::Threshold(img_0, img_threshold, 20, 255, HLS_THRESH_BINARY);
    //first iteration
    hls::Dilate(img_threshold, img_dilated1);
    //second iteration
    hls::Dilate(img_dilated1, img_dilated2);
    // Return result
    hls::Mat2AXIvideo(img_dilated2, result);
}
