#include <ap_fixed.h>
#include <ap_int.h>


struct narrow_stream {

	ap_uint<24> data;

	ap_uint<1> user;
	ap_uint<1> last;
};

struct wide_stream {
	ap_uint<32> data;
	ap_uint<1> user;
	ap_uint<1> last;
};

void hls_unpack_gray(wide_stream* stream_in_32, narrow_stream* stream_out_24)
{
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE axis depth=24 port=stream_in_32
#pragma HLS INTERFACE axis depth=96 port=stream_out_24

	bool last = false;

	while (!last)
	{
		#pragma HLS pipeline II=4
		ap_uint<32> data = stream_in_32->data;
		last = stream_in_32->last;
		ap_uint<1> user = stream_in_32->user;
		++stream_in_32;
		for (int i = 0; i < 4; ++i)
		{
			ap_uint<24> out_data = 0;
			out_data.range(7,0) = data.range(i*8 + 7, i*8);
			stream_out_24->data = out_data;
			stream_out_24->last = i == 3? last: 0;
			stream_out_24->user = i == 0? user: ap_uint<1>(0);
			++stream_out_24;
		}
	}
}
