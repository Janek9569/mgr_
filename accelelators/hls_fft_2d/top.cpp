#include "ap_fixed.h"
#include "hls_fft.h"
#include <stdint.h>
#include <math.h>

#define FFT_FWD 1
#define FFT_REV 0
// configurable params
const char FFT_INPUT_WIDTH                     = 16;
const char FFT_OUTPUT_WIDTH                    = 16;
const char FFT_CONFIG_WIDTH                    = 16;
const int  FFT_LENGTH                          = 64;


#include <complex>
using namespace std;

struct config1 : hls::ip_fft::params_t {
	static const unsigned complex_mult_type = 3;
	static const unsigned butterfly_type = 3;
	static const unsigned arch_opt = 1;
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned phase_factor_width = 24;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = 6;
};

typedef hls::ip_fft::config_t<config1> config_t;
typedef hls::ip_fft::status_t<config1> status_t;

typedef ap_fixed<FFT_INPUT_WIDTH,1> float16;

void prepare_input_buffer(complex<float16>* xn, uint8_t* src)
{
#pragma HLS PIPELINE
	for(int i = 0; i < FFT_LENGTH; ++i)
	    xn[i] = src[i];
}

void fft_wrapper(uint8_t *input_buffer, complex<float16> *output_buffer)
{
//#pragma HLS dataflow
	complex<float16> xn[FFT_LENGTH];
#pragma HLS interface ap_fifo port=xn
	//setup fft
	config_t fft_config;
	status_t fft_status;

#pragma HLS interface ap_fifo port=fft_config
#pragma HLS interface ap_fifo port=fft_status
	fft_config.setDir(FFT_FWD);
	fft_config.setSch(0x2AB);

	prepare_input_buffer(xn, input_buffer);
	hls::fft<config1>(xn, output_buffer, &fft_status, &fft_config);
}

void fft_wrapper_cmplx(complex<float16> *input_buffer, complex<float16> *output_buffer)
{
	config_t fft_config;
	status_t fft_status;
#pragma HLS interface ap_fifo port=fft_config
#pragma HLS interface ap_fifo port=fft_status
	fft_config.setDir(FFT_FWD);
	fft_config.setSch(0x2AB);

	hls::fft<config1>(input_buffer, output_buffer, &fft_status, &fft_config);
}

void ifft_wrapper(complex<float16> *input_buffer, complex<float16> *output_buffer)
{
	config_t fft_config;
	status_t fft_status;
#pragma HLS interface ap_fifo port=fft_config
#pragma HLS interface ap_fifo port=fft_status
	fft_config.setDir(FFT_REV);
	fft_config.setSch(0x2AB);

	hls::fft<config1>(input_buffer, output_buffer, &fft_status, &fft_config);
}

void fft_top(uint8_t *in_act, uint8_t *in_prev, uint8_t *out)
{
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE m_axi port=in_act offset=slave
#pragma HLS INTERFACE m_axi port=in_prev offset=slave
#pragma HLS INTERFACE m_axi port=out offset=slave

    uint8_t act_img[FFT_LENGTH][FFT_LENGTH];
    uint8_t prev_img[FFT_LENGTH][FFT_LENGTH];

    complex<float16> act_row_FFT[FFT_LENGTH][FFT_LENGTH];
    complex<float16> prev_row_FFT[FFT_LENGTH][FFT_LENGTH];

    complex<float16> col_vector_act[FFT_LENGTH];
    complex<float16> col_vector_prev[FFT_LENGTH];

    complex<float16> act_FFT_2D[FFT_LENGTH][FFT_LENGTH];
    complex<float16> prev_FFT_2D[FFT_LENGTH][FFT_LENGTH];

    complex<float16> mul[FFT_LENGTH][FFT_LENGTH];

#pragma HLS interface ap_fifo port=col_vector_act
#pragma HLS interface ap_fifo port=col_vector_prev
#pragma HLS interface ap_fifo port=prev_img

#pragma HLS interface ap_fifo port=act_FFT_2D
#pragma HLS interface ap_fifo port=prev_FFT_2D
#pragma HLS interface ap_fifo port=mul
#pragma HLS interface ap_fifo port=act_row_FFT
#pragma HLS interface ap_fifo port=prev_row_FFT

    //load images to BRAM
    for(int i = 0; i < FFT_LENGTH; ++i)
    {
    	memcpy(act_img[i], in_act + i*FFT_LENGTH, FFT_LENGTH);
    	memcpy(prev_img[i], in_prev + i*FFT_LENGTH, FFT_LENGTH);
    }

    //FFT by row
    for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma HLS PIPELINE
    	fft_wrapper(act_img[i], act_row_FFT[i]);
		fft_wrapper(prev_img[i], prev_row_FFT[i]);
	}

    //FFT by col
    for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma HLS PIPELINE
    	//construct column vectors
    	for(int j = 0; j < FFT_LENGTH; ++j)
    	{
    		col_vector_act[j] = act_row_FFT[j][i];
    		col_vector_prev[j] = prev_row_FFT[j][i];
    	}

    	fft_wrapper_cmplx(col_vector_act, act_FFT_2D[i]);
    	fft_wrapper_cmplx(col_vector_prev, prev_FFT_2D[i]);
	}

    //spectrum multiplication with conjugate
    for(int i = 0; i < FFT_LENGTH; ++i)
    {
		//mul spectrums with conjugate
		for(int itr = 0; itr < FFT_LENGTH; ++itr)
		{
			#pragma HLS PIPELINE
			float16 real_act = act_FFT_2D[i][itr].real();
			float16 imag_act = act_FFT_2D[i][itr].imag();
			float16 real_prv = prev_FFT_2D[i][itr].real();
			float16 imag_prv = -1 * prev_FFT_2D[i][itr].imag();
			float16 real_mag = real_act * real_prv - imag_act * imag_prv;
			float16 imag_mag = real_act * imag_prv + imag_act * real_prv;

			//save result and transpose
			mul[itr][i].real(real_mag);
			mul[itr][i].imag(imag_mag);
		}
    }

    //inverse FFT by row
    for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma HLS PIPELINE
    	ifft_wrapper(mul[i], act_row_FFT[i]);
		ifft_wrapper(mul[i], prev_row_FFT[i]);
	}

    //inverse FFT by col and find max ind
    float16 max_value = 0;
    uint8_t ind_x, ind_y;
    for(int i = 0; i < FFT_LENGTH; ++i)
	{
    	//construct column vectors
    	for(int j = 0; j < FFT_LENGTH; ++j)
    	{
			#pragma HLS PIPELINE
    		col_vector_act[j] = act_row_FFT[j][i];
    		col_vector_prev[j] = prev_row_FFT[j][i];
    	}

    	ifft_wrapper(col_vector_act, act_FFT_2D[i]);
    	ifft_wrapper(col_vector_prev, prev_FFT_2D[i]);

    	//get max value
    	for(int j = 0; j < FFT_LENGTH; ++j)
    	{
    		if(act_FFT_2D[i][j].real() > max_value)
    		{
				#pragma HLS PIPELINE
    			max_value = act_FFT_2D[i][j].real();
    			ind_x = i;
    			ind_y = j;
    		}
    	}
	}

    *out = ind_x;
    *(out+1) = ind_y;
}










