#include "TrivialAlg.h"
#include <opencv2/imgproc/imgproc.hpp>


TrivialAlg::TrivialAlg(Mat& frame):
	MovDetAlg()
{
	/* Convert ref frame to grayscale */
	cvtColor(frame, framePrev, CV_BGR2GRAY);

	/* Gaussian filtering */
	GaussianBlur(framePrev, framePrev, cv::Size(0, 0), GAUSSIAN_SIGMA_X, GAUSSIAN_SIGMA_Y, BORDER_DEFAULT);
}


TrivialAlg::~TrivialAlg()
{
}

Mat TrivialAlg::calculate(Mat& newFrame)
{
	static RNG rng(12345);
	Mat newFrameGrayscale;
	Mat diffFrame;
	vector<vector<Point>> contours;

	/* Convert act frame to grayscale */
	cvtColor(newFrame, newFrameGrayscale, CV_BGR2GRAY);

	/* Gaussian filtering */
	GaussianBlur(newFrameGrayscale, newFrameGrayscale, cv::Size(11, 11), 0, 0);

	/* Diff */
	absdiff(newFrameGrayscale, framePrev, diffFrame);

	/* Threshold */
	threshold(diffFrame, diffFrame, 15, 255, THRESH_BINARY);

	/* Dilate to fill the holes */
	Mat element = getStructuringElement(MORPH_RECT, Size(3, 3));
	dilate(diffFrame, diffFrame, element, Point(-1, -1), 2);

	/* Find countours */
	findContours(diffFrame, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	for (int i = 0; i < contours.size(); ++i)
	{
		if (contourArea(contours[i]) < 300)
			continue;

		auto rect = boundingRect(contours[i]);
		rectangle(newFrame, rect, (255, 255, 0));
	}
	/* Update previuous frame */
	framePrev = newFrameGrayscale;
	return diffFrame;
}
