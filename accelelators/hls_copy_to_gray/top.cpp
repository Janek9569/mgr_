#include <ap_fixed.h>
#include <ap_int.h>

typedef ap_uint<8> pixel_type;
typedef ap_int<8> pixel_type_s;
typedef ap_ufixed<8,0, AP_RND, AP_SAT> comp_type;
typedef ap_fixed<10,2, AP_RND, AP_SAT> coeff_type;
struct video_stream {
	struct {
		pixel_type_s p1;
		pixel_type_s p2;
		pixel_type_s p3;
	} data;
	ap_uint<1> user;
	ap_uint<1> last;
};


void hls_convert_to_gray(video_stream* stream_in_24, video_stream* stream_out_24)
{
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=stream_in_24
#pragma HLS INTERFACE axis port=stream_out_24

#pragma HLS pipeline II=1

	stream_out_24->user = stream_in_24->user;
	stream_out_24->last = stream_in_24->last;
	comp_type in1, in2, in3, out1, out2, out3;
	coeff_type c1 = 0.2126;
	coeff_type c2 = 0.7152;
	coeff_type c3 = 0.0722;
	in1.range() = stream_in_24->data.p1;
	in2.range() = stream_in_24->data.p2;
	in3.range() = stream_in_24->data.p3;

	out1 = in1 * c1 + in2 * c2 + in3 * c3;

	stream_out_24->data.p1 = out1.range();
	stream_out_24->data.p2 = 0;
	stream_out_24->data.p3 = 0;
}

