\babel@toc {english}{}
\contentsline {chapter}{Nomenclature}{0}{chapter*.1}% 
\contentsline {chapter}{\numberline {1}Introduction}{4}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Purpose and scope of the work}{4}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Typical usages}{5}{section.1.2}% 
\contentsline {chapter}{\numberline {2}Introduction to motion detection}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Input data - characteristic features}{6}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Movement detection terminology [1,3]}{7}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Problems connected with movement detection [2,7]}{7}{section.2.3}% 
\contentsline {chapter}{\numberline {3}Literature research}{11}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Classification of movement detection methods}{11}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Differential methods [4,5]}{11}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Gradient methods [6,8]}{12}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Correlation methods [9,10,11,12]}{13}{subsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.1.4}Frequency methods [8,13]}{15}{subsection.3.1.4}% 
\contentsline {section}{\numberline {3.2}Review of popular algorithms}{16}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Simple movement detection}{16}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Simple movement detection with filtration and result enhancement}{16}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Phase correlation only [14]}{17}{subsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.2.4}Other algorithms}{18}{subsection.3.2.4}% 
\contentsline {subsubsection}{Horn and Schunck algorithm [15]}{18}{section*.35}% 
\contentsline {subsubsection}{Lucas and Kanade algorithm [16]}{19}{section*.38}% 
\contentsline {subsubsection}{Nagel algorithm [17]}{19}{section*.42}% 
\contentsline {subsubsection}{Anandan algorithm [10]}{19}{section*.44}% 
\contentsline {subsubsection}{Fleet and Jepson [8]}{21}{section*.49}% 
\contentsline {chapter}{\numberline {4}Environment setup}{22}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Expectations}{22}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Hardware platform}{22}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Starting point - block diagram}{24}{section.4.3}% 
\contentsline {chapter}{\numberline {5}Algorithm implementations}{26}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Simple movement detection with and without filtration and result enhancement}{26}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}Accelerators}{26}{subsection.5.1.1}% 
\contentsline {subsubsection}{Colour converter}{26}{section*.59}% 
\contentsline {subsubsection}{Pixel pack and Pixel unpack}{27}{section*.62}% 
\contentsline {subsubsection}{Threshold}{27}{section*.64}% 
\contentsline {subsubsection}{Gaussian blur}{27}{section*.67}% 
\contentsline {subsubsection}{Dilatation}{29}{section*.73}% 
\contentsline {subsubsection}{Absolute difference}{30}{section*.77}% 
\contentsline {section}{\numberline {5.2}Phase correlation only - sparse optical flow}{33}{section.5.2}% 
\contentsline {subsection}{\numberline {5.2.1}Accelerators}{33}{subsection.5.2.1}% 
\contentsline {subsubsection}{Forward/Inverse Fourier transform with result transposition and argmax}{33}{section*.85}% 
\contentsline {subsubsection}{Complex multiplication with conjugate}{34}{section*.90}% 
\contentsline {chapter}{\numberline {6}Tests and results}{35}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Resource and timing requirements}{35}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Speed}{37}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Simple movement detection with and without enhance}{37}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}Phase correlation only}{37}{subsection.6.2.2}% 
\contentsline {chapter}{\numberline {7}Summary}{40}{chapter.7}% 
\contentsline {subsection}{\numberline {7.0.1}Assessment of results}{40}{subsection.7.0.1}% 
\contentsline {chapter}{Bibliography}{40}{subsection.7.0.1}% 
