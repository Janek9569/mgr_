#include "ap_fixed.h"
#include "hls_fft.h"
#include <stdint.h>
#include <math.h>

#define FFT_FWD 1
#define FFT_REV 0
// configurable params
const char FFT_INPUT_WIDTH                     = 32;
const char FFT_OUTPUT_WIDTH                    = FFT_INPUT_WIDTH;
const char FFT_CONFIG_WIDTH                    = 16;
const int  FFT_LENGTH                          = 64;

#include <complex>
using namespace std;

struct config1 : hls::ip_fft::params_t {
	static const unsigned arch_opt = 2;
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned phase_factor_width = 24;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = 6;
};

typedef hls::ip_fft::config_t<config1> config_t;
typedef hls::ip_fft::status_t<config1> status_t;

void prepare_input_buffer(complex<float>* xn, uint8_t* src)
{
#pragma HLS PIPELINE
	for(int i = 0; i < FFT_LENGTH; ++i)
	    xn[i] = src[i];
}

void prepare_output_buffer(complex<float>* xk, float* buffer)
{
	int itr = 0;
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		buffer[itr++] = xk[i].real();
		buffer[itr++] = xk[i].imag();
	}
}

void hls_fft_C(uint8_t *in, float *out)
{
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE m_axi port=in offset=slave
#pragma HLS INTERFACE m_axi port=out offset=slave

	complex<float> xn[FFT_LENGTH];
	complex<float> xk[FFT_LENGTH];

	uint8_t input_buffer[FFT_LENGTH];
	float output_buffer[FFT_LENGTH * 2];

	status_t fft_status;

	L1:for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma AP dataflow
		config_t fft_config;

		//setup fft
		fft_config.setDir(FFT_FWD);
		fft_config.setSch(0x2AB);

		//copy data to bram
		memcpy(input_buffer, in + i * FFT_LENGTH, FFT_LENGTH);

		L2:for(int i = 0; i < FFT_LENGTH; ++i)
			xn[i] = input_buffer[i];

		hls::fft<config1>(xn, xk, &fft_status, &fft_config);

		int itr = 0;
		L3:for(int i = 0; i < FFT_LENGTH; ++i)
		{
			output_buffer[itr++] = xk[i].real();
			output_buffer[itr++] = xk[i].imag();
		}

		//copy data to DDR
		memcpy(out + i * FFT_LENGTH, output_buffer, FFT_LENGTH*sizeof(float)*2);
	}
}










