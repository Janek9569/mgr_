#include "MovDetAlg.h"
#include "TrivialAlg.h"
#include "CorrelationOnly.h"
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

int main()
{
	Mat frame1 = imread("Base.png");
	Mat frame2 = imread("Moved.png");
	CorrelationOnly correlationOnlyTest(frame1);
	correlationOnlyTest.calculate(frame2);


	return 0;
	VideoCapture cap(0);

	/* Valid input test */
	if (!cap.isOpened())
	{
		cout << "Could not open or find the mp4" << std::endl;
		return -1;
	}

	/* Create window for display */
	namedWindow("Display window", WINDOW_AUTOSIZE);

	/* Catch first frame */
	Mat frame;
	cap >> frame;
	if (frame.empty())
	{
		cout << "Could not load first frame" << std::endl;
		return -2;
	}

	/* Init algorithms */
	CorrelationOnly correlationOnly(frame);
	while (1) 
	{
		/* Capture frame-by-frame */
		cap >> frame;

		/* If the frame is empty, break immediately */
		if (frame.empty())
			break;

		/* Pass frame to the alg */
		Mat resultFrame = correlationOnly.calculate(frame);

		/* Display the resulting frame */
		imshow("FrameRef", frame);
		imshow("FrameCalculated", resultFrame);

		/* Press  ESC on keyboard to exit */
		char c = (char)waitKey(25);
		if (c == 27)
			break;
	}

	/* Release capture */
	cap.release();

	/* Close all frames */
	destroyAllWindows();

	waitKey(0);
	return 0;
}