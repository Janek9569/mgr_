#include "ap_fixed.h"
#include "hls_fft.h"
#include <stdint.h>
#include <math.h>

#define FFT_FWD 1
#define FFT_REV 0
// configurable params
const char FFT_INPUT_WIDTH                     = 32;
const char FFT_OUTPUT_WIDTH                    = FFT_INPUT_WIDTH;
const char FFT_CONFIG_WIDTH                    = 16;
const int  FFT_LENGTH                          = 64;

#include <complex>
using namespace std;

struct config1 : hls::ip_fft::params_t {
	static const unsigned arch_opt = 2;
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned phase_factor_width = 24;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = 6;
};

struct config2 : hls::ip_fft::params_t {
	static const unsigned arch_opt = 2;
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned phase_factor_width = 24;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = 6;
};

typedef hls::ip_fft::config_t<config1> config_t1;
typedef hls::ip_fft::config_t<config2> config_t2;
typedef hls::ip_fft::status_t<config1> status_t1;
typedef hls::ip_fft::status_t<config2> status_t2;

void prepare_input_buffer(complex<float>* xn, uint8_t* src)
{
#pragma HLS PIPELINE
	for(int i = 0; i < FFT_LENGTH; ++i)
	    xn[i] = src[i];
}

void hls_fft_C(uint8_t *in_act, uint8_t *in_prev, float *out1, float *out2)
{
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE m_axi port=in_act offset=slave
#pragma HLS INTERFACE m_axi port=in_prev offset=slave
#pragma HLS INTERFACE m_axi port=out1 offset=slave
#pragma HLS INTERFACE m_axi port=out2 offset=slave

	complex<float> xn1[FFT_LENGTH];
	complex<float> xn2[FFT_LENGTH];
	complex<float> act_fft_row[FFT_LENGTH][FFT_LENGTH];

	uint8_t input_buffer1[FFT_LENGTH];
	uint8_t input_buffer2[FFT_LENGTH];
	float output_buffer1[FFT_LENGTH * 2];
	float output_buffer2[FFT_LENGTH * 2];

	//act img fft
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma AP dataflow
		config_t1 fft_config;
		status_t1 fft_status;

		//setup fft
		fft_config.setDir(FFT_FWD);
		fft_config.setSch(0x2AB);

		//copy data to bram
		memcpy(input_buffer1, in_act + i * FFT_LENGTH, FFT_LENGTH);

		prepare_input_buffer(xn1, input_buffer1);
		hls::fft<config1>(xn1, act_fft_row[i], &fft_status, &fft_config);
		int itr = 0;
		for(int j = 0; j < FFT_LENGTH; ++j)
		{
			output_buffer1[itr++] = act_fft_row[i][j].real();
			output_buffer1[itr++] = act_fft_row[i][j].imag();
		}

		//copy data to DDR
		memcpy(out1 + i * FFT_LENGTH*2, output_buffer1, FFT_LENGTH*sizeof(float)*2);
	}

	complex<float> prev_fft_row[FFT_LENGTH][FFT_LENGTH];
	//prev img fft
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma AP dataflow
		config_t2 fft_config;
		status_t2 fft_status;

		//setup fft
		fft_config.setDir(FFT_FWD);
		fft_config.setSch(0x2AB);

		//copy data to bram
		memcpy(input_buffer2, in_prev + i * FFT_LENGTH, FFT_LENGTH);

		prepare_input_buffer(xn2, input_buffer2);
		hls::fft<config2>(xn2, prev_fft_row[i], &fft_status, &fft_config);
		int itr = 0;
		for(int j = 0; j < FFT_LENGTH; ++j)
		{
			output_buffer2[itr++] = prev_fft_row[i][j].real();
			output_buffer2[itr++] = prev_fft_row[i][j].imag();
		}

		//copy data to DDR
		memcpy(out2 + i * FFT_LENGTH*2, output_buffer2, FFT_LENGTH*sizeof(float)*2);
	}
}










