#include "ap_fixed.h"
#include "hls_fft.h"
#include <stdint.h>
#include <math.h>

#define FFT_FWD 1
#define FFT_REV 0
// configurable params
const char FFT_INPUT_WIDTH                     = 32;
const char FFT_OUTPUT_WIDTH                    = FFT_INPUT_WIDTH;
const char FFT_CONFIG_WIDTH                    = 8;
const int  FFT_LENGTH                          = 64;

typedef float MyFloat;
//typedef ap_fixed<FFT_INPUT_WIDTH,1> MyFloat;
#include <complex>
using namespace std;

struct config1 : hls::ip_fft::params_t {
	static const unsigned input_width = 32;
	static const unsigned output_width = 32;
	static const unsigned arch_opt = 1;
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned phase_factor_width = 24;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = 6;
};

struct config2 : hls::ip_fft::params_t {
	static const unsigned input_width = 32;
	static const unsigned output_width = 32;
	static const unsigned arch_opt = 1;
    static const unsigned ordering_opt = hls::ip_fft::natural_order;
    static const unsigned phase_factor_width = 24;
    static const unsigned config_width = FFT_CONFIG_WIDTH;
    static const unsigned max_nfft = 6;
};

typedef hls::ip_fft::config_t<config1> config_t1;
typedef hls::ip_fft::config_t<config2> config_t2;
typedef hls::ip_fft::status_t<config1> status_t1;
typedef hls::ip_fft::status_t<config2> status_t2;

void prepare_input_buffer(complex<float>* xn, float* src)
{
#pragma HLS PIPELINE
	int itr = 0;
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma HLS PIPELINE
	    xn[i].real(src[itr++]);
	    xn[i].imag(src[itr++]);
	}
}

void hls_fft_row_transpose(float *in_act, float *in_prev, float *out1, float *out2)
{
#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS INTERFACE m_axi port=in_act offset=slave
#pragma HLS INTERFACE m_axi port=in_prev offset=slave
#pragma HLS INTERFACE m_axi port=out1 offset=slave
#pragma HLS INTERFACE m_axi port=out2 offset=slave

	complex<MyFloat> xn1[FFT_LENGTH];
	complex<MyFloat> xn2[FFT_LENGTH];
	complex<MyFloat> act_fft_row[FFT_LENGTH][FFT_LENGTH];
	complex<MyFloat> act_fft_row_trans[FFT_LENGTH][FFT_LENGTH];

	float input_buffer1[FFT_LENGTH*2];
	float input_buffer2[FFT_LENGTH*2];

	//act img fft
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma AP dataflow
		config_t1 fft_config;
		status_t1 fft_status;

		//setup fft
		fft_config.setDir(FFT_FWD);
		fft_config.setSch(0x2AB);

		//copy data to bram
		memcpy(input_buffer1, in_act + i * FFT_LENGTH * 2, FFT_LENGTH * sizeof(float) * 2);
		prepare_input_buffer(xn1, input_buffer1);
		hls::fft<config1>(xn1, act_fft_row[i], &fft_status, &fft_config);

		for(int j = 0; j < FFT_LENGTH; ++j)
		{
			act_fft_row_trans[j][i] = act_fft_row[i][j];
		}
	}
	float output_buffer1[FFT_LENGTH*FFT_LENGTH * 2];

	int itr1 = 0;
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		for(int j = 0; j < FFT_LENGTH; ++j)
		{
			output_buffer1[itr1++] = act_fft_row_trans[i][j].real();
			output_buffer1[itr1++] = act_fft_row_trans[i][j].imag();
		}
	}

	memcpy(out1, output_buffer1, FFT_LENGTH*FFT_LENGTH*sizeof(float)*2);

	complex<MyFloat> prev_fft_row[FFT_LENGTH][FFT_LENGTH];
	complex<MyFloat> prev_fft_row_trans[FFT_LENGTH][FFT_LENGTH];
	//prev img fft
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		#pragma AP dataflow
		config_t2 fft_config;
		status_t2 fft_status;

		//setup fft
		fft_config.setDir(FFT_FWD);
		fft_config.setSch(0x2AB);

		//copy data to bram
		memcpy(input_buffer2, in_prev + i * FFT_LENGTH * 2, FFT_LENGTH * sizeof(float) * 2);

		prepare_input_buffer(xn2, input_buffer2);
		hls::fft<config2>(xn2, prev_fft_row[i], &fft_status, &fft_config);
		for(int j = 0; j < FFT_LENGTH; ++j)
		{
			prev_fft_row_trans[j][i] = prev_fft_row[i][j];
		}
	}
	float output_buffer2[FFT_LENGTH*FFT_LENGTH * 2];

	int itr2 = 0;
	for(int i = 0; i < FFT_LENGTH; ++i)
	{
		for(int j = 0; j < FFT_LENGTH; ++j)
		{
			output_buffer2[itr2++] = prev_fft_row_trans[i][j].real();
			output_buffer2[itr2++] = prev_fft_row_trans[i][j].imag();
		}
	}

	memcpy(out2, output_buffer2, FFT_LENGTH*FFT_LENGTH*sizeof(float)*2);
}










