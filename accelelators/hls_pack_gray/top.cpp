#include <ap_fixed.h>
#include <ap_int.h>

typedef ap_uint<8> pixel_type;
typedef ap_int<8> pixel_type_s;

struct narrow_stream {

	ap_uint<24> data;

	ap_uint<1> user;
	ap_uint<1> last;
};

struct wide_stream {
	ap_uint<32> data;
	ap_uint<1> user;
	ap_uint<1> last;
};


void hls_pack_gray(narrow_stream* stream_in_24, wide_stream* stream_out_32)
{
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE axis depth=24 port=stream_in_24
#pragma HLS INTERFACE axis depth=24 port=stream_out_32

	bool last = false;
	bool delayed_last = false;

	while (!delayed_last)
	{
		#pragma HLS pipeline II=4
		delayed_last = last;
		bool user = false;
		ap_uint<32> data;
		for (int i = 0; i < 4; ++i)
		{
			if (!last)
			{
				user |= stream_in_24->user;
				last = stream_in_24->last;
				data.range(i*8 + 7, i * 8) = stream_in_24->data.range(7,0);
				++stream_in_24;
			}
		}
		if (!delayed_last)
		{
			stream_out_32->user = user;
			stream_out_32->last = last;
			stream_out_32->data = data;
			++stream_out_32;
		}
	}
}
