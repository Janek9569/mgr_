
#include "hls_video.h"

// maximum image size
#define MAX_WIDTH  1920
#define MAX_HEIGHT 1080

// typedef video library core structures (set to 8 bits -- 1 pixel -- per beat)
typedef hls::stream<ap_axiu<24,1,1,1> >                AXI_STREAM;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3>      BGR_IMAGE;

void hls_gauss(AXI_STREAM& img, AXI_STREAM& result)
{
    //Create AXI streaming interfaces for the core
#pragma HLS CLOCK domain=default
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis port=img
#pragma HLS INTERFACE axis port=result

	BGR_IMAGE img_0(1080, 1920);
	BGR_IMAGE img_gauss(1080, 1920);


#pragma HLS dataflow

    hls::AXIvideo2Mat(img, img_0);
	// Take difference between frames
    hls::GaussianBlur<5,5>(img_0, img_gauss);
    // Return result
    hls::Mat2AXIvideo(img_gauss, result);
}
