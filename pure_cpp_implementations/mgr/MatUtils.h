#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

struct ValueWithPos
{
	Point2d point;
	int value;
};

class MatUtils
{
public:
	MatUtils();
	~MatUtils();
	static void printMat(Mat& mat, std::string fileName = "", std::string matName = "");
	static std::vector<ValueWithPos> findBiggestValues(Mat& mat, int num);
	static std::vector<Mat> divideImage(Mat& mat, int xSize, int ySize);

private:
	struct ValueWithPosCmp
	{
		bool operator() (const ValueWithPos& lhs, const ValueWithPos& rhs) const
		{
			return lhs.value < rhs.value;
		}
	};
};

